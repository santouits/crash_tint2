#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>

// run make or compile with `gcc main.c -lX11`
 
// Change these numbers to test
int ICON_WIDTH = 2000;
int ICON_HEIGHT = 2000;

int main(void) {
    Display *d;
    Window w;
    XEvent e;
    int s;

    d = XOpenDisplay(NULL);
    if (d == NULL) {
        fprintf(stderr, "Cannot open display\n");
        exit(1);
    }

    s = DefaultScreen(d);
    w = XCreateSimpleWindow(d, RootWindow(d, s), 10, 10, 100, 100, 1, BlackPixel(d, s), WhitePixel(d, s));
    XSelectInput(d, w, ExposureMask | KeyPressMask);

    // Create a big icon image
    int length = 2 + ICON_WIDTH * ICON_HEIGHT;
    unsigned long *buffer = (unsigned long*) malloc(length * sizeof(*buffer));
    buffer[0] = ICON_WIDTH;
    buffer[1] = ICON_HEIGHT;
    for (int i = 2; i < length; ++i) {
        buffer[i] = (int)0xFFFFC0CB;
    }

    Atom net_wm_icon = XInternAtom(d, "_NET_WM_ICON", False);
    Atom cardinal = XInternAtom(d, "CARDINAL", False);

    XChangeProperty(d, w, net_wm_icon, cardinal, 32, PropModeReplace, (const unsigned char*) buffer, length);

    XMapWindow(d, w);

    while (1) {
        XNextEvent(d, &e);
        if (e.type == KeyPress)
            break;
    }

    XCloseDisplay(d);
    return 0;
}
